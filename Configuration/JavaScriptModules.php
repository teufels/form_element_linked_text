<?php

return [
    'dependencies' => ['form'],
    'imports' => [
        '@teufels/form-element-linked-text/' => 'EXT:form_element_linked_text/Resources/Public/JavaScript/',
    ],
];
