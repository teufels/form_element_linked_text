[![VENDOR](https://img.shields.io/badge/vendor-teufels-blue.svg)](https://bitbucket.org/teufels/workspace/projects/TTER)
[![PACKAGE](https://img.shields.io/badge/package-form--element--linked--text-orange.svg)](https://bitbucket.org/teufels/form_element_linked_text/src/main/)
[![KEY](https://img.shields.io/badge/extension--key-form--element--linked--text-red.svg)](https://bitbucket.org/teufels/form_element_linked_text/src/main/)
![version](https://img.shields.io/badge/version-3.0.*-yellow.svg?style=flat-square)

[ ṯeufels ] Custom form element "Linked text"
==========

This TYPO3 extension adds a custom form element "Linked text" to the
TYPO3 form framework. The user is able to define the link target and the
link text.

Based on the Extension https://extensions.typo3.org/extension/form_element_linked_text

***
#### This version supports TYPO3
![TYPO3Version](https://img.shields.io/badge/12_LTS-%23A6C694.svg?style=flat-square)
![TYPO3Version](https://img.shields.io/badge/13_LTS-%23A6C694.svg?style=flat-square)

### Composer support
`composer req teufels/form-element-linked-text`

***

## How to use (Quick-Guide)
- Install with composer
- (Optional) Import Static Template (before sitepackage)
  - will be automatically imported - on problems deactivate this in the EXT Settings and manually import

***

## Customization

The extension overrides templates for the following views:
* email to receiver, plain text
* email to receiver, HTML
* email to sender, plain text
* email to sender, HTML
* summary page

This is necessary to render links correctly. By default, the core templates
of the form framework escape any HTML in both email and plain text mails.
Thus, your users would receive mails with broken links.

If you also override those templates, please adopt your files accordingly.

***

## Usage

Open the TYPO3 form editor and create a new form/open an existing one. Add
a new element to your form. The modal will list the new custom form element
"Linked text". Provide a label for the static text including the link text.
Select a page you want to link to.

### Combination of label and link

The default label consists of the label itself, followed by a link to the
specified page with the given link text.

Example:

* Label: `I accept the `
* Link text: `terms and conditions.`
* Output: `I accept the <a href="/terms" target="_blank">terms and conditions.</a>`

If want to use the link inside your label, define the link position
in the label with a character substitution. We highly **recommend** this way.

Example:

* Label: `I have read the %s and accept them.`
* Link text: `terms and conditions`
* Output: `I have read the <a href="/terms" target="_blank">terms and conditions</a> and accept them.`

You can also use more than one link in the text label. For this, just
use the field `additionalLinks` and provide a combination of Page UID and
link text.

Example:

* Label: `I have read the %s and %s and accept them.`
* Link text: `terms and conditions`
* Additional links:
  - `privacy policy`
* Output: `I have read the <a href="/terms" target="_blank">terms and conditions</a> and <a href="/privacy-policy" target="_blank">privacy policy</a> and accept them.`

#### Link configuration

You can provide additional link configuration which will be used when
generating the link within the label. Note that this can only be defined
in the appropriate `.form.yaml` file but not in the form editor and
applies to all generated links.

```yaml
type: LinkedText
identifier: consent
label: 'I accept the %s and %s.'
properties:
  pageUid: '67'
  linkText: 'terms and conditions'
  additionalLinks:
    83: 'privacy policy'

renderingOptions:
  linkConfiguration:
    # Additional typolink configuration can be inserted here, e.g.:
    no_cache: 1
```

For a full list of available configuration take a look at the
[TypoScript reference](https://docs.typo3.org/m/typo3/reference-typoscript/master/en-us/Functions/Typolink.html).

#### Override default link target

By default, the link target is set to `_blank`. If you want to override it,
just define a custom link configuration `parameter` – either an empty string
or a custom target/additional parameter configuration:

```yaml
renderingOptions:
  linkConfiguration:
    parameter: ''
```

***

## Possible improvements or changes

Instead of creating a new form element, the existing `Text` form element
could have been extended. In order to provide a more complex example, the
extension creates a new element.

At the time of writing this, you have to provide a small JavaScript snippet
(see `\Resources\Public\JavaScript\Backend\FormEditor\ViewModel.js`). This
snippet is needed to show the custom form element in the form editor. For
future TYPO3 versions we are aiming to remove this stumbling block to smoothen
the element registration.

***

## Versions

| News | TYPO3   | PHP       | Notes                                            |
|------|---------|-----------|--------------------------------------------------|
| main | 12 - 13 | ^8.1      |                                                  |
| 3.x  | 12 - 13 | ^8.1      | Breaking changes. See comments below.            |
| 2.x  | 12      | 7.4 - 8.3 | [!!!]Breaking changes. See comments below.       |
| 1.x  | 11      | 7.4 - 8.2 | inital from tritum/form-element-linked-checkbox  |

### Breaking changes version 3.x
* [!!!][CHANGE] Drop support for PHP 7.4

### Breaking changes version 2.x
* [!!!][CHANGE] Drop support for TYPO3 v11
* [!!!][CHANGE] Vendor changed from HIVE to T3teufels
* [!!!][CHANGE] composer name changed from `"beewilly/form-element-linked-text"` to `"teufels/form-element-linked-text"`

***

## Update & Migration from beewilly/form-element-linked-text
1. in composer.json replace `beewilly/form-element-linked-text` with `"teufels/form-element-linked-text":"^2.0"`
2. Composer update
3. if BE label for Field Settings missing add `2687159009: 'EXT:form_element_linked_text/Resources/Private/Language/Database.xlf'` at line [13] to BasicFormSetup.yaml in sitepackage 

***

## Credits

The TYPO3 extension "form_element_linked_text" was created by [Björn Jacob](https://www.tritum.de) on which this extension is based on.