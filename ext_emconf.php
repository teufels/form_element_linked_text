<?php

$EM_CONF['form_element_linked_text'] = [
    'title' => 'Form: Linked text element',
    'description' => 'Adds a new form element which allows the editor to create a text with a linked label.',
    'category' => 'fe',
    'state' => 'stable',
    'clearCacheOnLoad' => 0,
    'author' => 'teufels GmbH, Björn Jacob, Elias Häußler',
    'author_email' => 'digital@teufels.com, bjoern.jacob@tritum.de, elias@haeussler.dev',
    'version' => '3.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '12.4.0-13.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
