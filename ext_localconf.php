<?php

defined('TYPO3') or die();

\Teufels\FormElementLinkedText\Configuration\Extension::addTypoScriptSetup();
\Teufels\FormElementLinkedText\Configuration\Extension::registerHooks();
